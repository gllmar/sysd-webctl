#!/bin/bash

# Get script absolute path
SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

echo "## Getting started in $SCRIPT_PATH"

# Get dependencies
read -p "Get dependencies? (y/n): " choice
if [[ $choice == "y" || $choice == "Y" ]]; then
    sudo apt install python3-gunicorn python3-flask gunicorn
fi

# Create service
read -p "Create service? (y/n): " choice
if [[ $choice == "y" || $choice == "Y" ]]; then
    # Prompt user for port number, default to 8010 if no answer or if the answer is not an integer
    read -p "Enter port number (default 8010): " PORT
    if [[ -z "$PORT" || ! "$PORT" =~ ^[0-9]+$ ]]; then
        PORT=8010
    fi
    
    service_file="/etc/systemd/system/sysd-webctl.service"  # Updated service file name
    working_directory="$SCRIPT_PATH"  # Updated to use single dollar sign $

    cat <<EOL | sudo tee $service_file
[Unit]
Description=sysd-webctl  # Updated description
After=network.target

[Service]
User=root
WorkingDirectory=$working_directory
ExecStart=/usr/bin/gunicorn -w 4 -b 0.0.0.0:$PORT app:app
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
EOL
    sudo systemctl daemon-reload
    sudo systemctl restart sysd-webctl
    read -p "Enable sysd-webctl (y/n): " choice
    if [[ $choice == "y" || $choice == "Y" ]]; then
        sudo systemctl enable sysd-webctl
    fi
fi


# Create NGINX route
read -p "Create NGINX route in nginx/conf.d? (y/n): " choice
if [[ $choice == "y" || $choice == "Y" ]]; then
    nginx_config_file="/etc/nginx/conf.d/sysd_webctl_forwarding"
    echo "Creating NGINX configuration for code forwarding..."
    
    cat <<EOL | sudo tee $nginx_config_file
location ~ ^/sysd-webgui(/.*)?$ {
    # Use return for a straightforward redirect
   return 302 http://\$host:$PORT;
}
location ~ ^/sysd(/.*)?$ {
    # Use return for a straightforward redirect
   return 302 http://\$host:$PORT;
}
location ~ ^/systemd(/.*)?$ {
    # Use return for a straightforward redirect
    return 302 http://\$host:$PORT;
}
EOL

    # Test NGINX configuration and reload if successful
    if sudo nginx -t; then
        echo "Reloading NGINX..."
        sudo systemctl reload nginx
    else
        echo "Failed to configure NGINX. Please check the configuration."
    fi
fi

echo "### Setup complete"
