from flask import Flask, render_template, jsonify, request
import subprocess
import socket
import json

app = Flask(__name__)

# Define a custom Jinja2 filter to calculate the length
def calculate_length(value):
    return len(value) if isinstance(value, list) else 0

# Add the custom filter to the Jinja2 environment
app.jinja_env.filters['length'] = calculate_length

# Load services from services.json

with open('services.json', 'r') as file:
    SERVICES_DATA = json.load(file)
    SERVICES = {}
    for k, v in SERVICES_DATA.items():
        if len(v) == 2:
            SERVICES[k] = v[0]
        else:
            SERVICES[k] = v[0]


def run_systemctl_command(command, service_name, service_type):
    """Executes a systemctl command and returns the output."""
    if command == "full-status":
        if service_type == "root":
            cmd = ["systemctl", "status", service_name]
        elif service_type == "user":
            dbus_cmd = f"DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus systemctl --user status {service_name}"
            cmd = ["su", "-", "pi", "-c", dbus_cmd]
        else:
            print("Error: Invalid service type")
            return [], "Invalid service type"
    elif service_type == "root":
        cmd = ["systemctl", command, service_name]
    elif service_type == "user":
        dbus_cmd = f"DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus systemctl --user {command} {service_name}"
        if command != "is-active":
            dbus_cmd += ".service"
        cmd = ["su", "-", "pi", "-c", dbus_cmd]
    else:
        print("Error: Invalid service type")  # Debugging line
        return [], "Invalid service type"  # Return empty command list and error message

    print(f"Executing command: {' '.join(cmd)}")  # Debugging line to print the command
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True)
    print(f"Command output: {result.stdout}")  # Debugging line to print the command output

    return cmd, result.stdout.strip()

@app.route('/')
def index():
    hostname = socket.gethostname()
    statuses = {}
    for service, service_type in SERVICES.items():
        status = run_systemctl_command("is-active", service, service_type)
        statuses[service] = {"status": status, "type": service_type}
    return render_template("index.html", services=statuses, hostname=hostname, SERVICES_DATA=SERVICES_DATA)

@app.route('/action', methods=["POST"])
def action():
    service_name = request.form.get("service_name")
    service_type = request.form.get("service_type")
    action = request.form.get("action")

    if action in ["start", "stop", "restart", "enable", "disable", "full-status"]:  # Add "full-status" to the list
        cmd, output = run_systemctl_command(action, service_name, service_type)
        return jsonify({"status": "success", "message": output, "cmd": ' '.join(cmd)})
    return jsonify({"status": "error", "message": "Invalid action"})

@app.route('/status', methods=["GET"])
def status():
    service = request.args.get('service')
    service_type = request.args.get('service_type')

    # If specific service is provided, only return its status
    if service and service_type:
        status = run_systemctl_command("is-active", service, service_type)[1]  # Get only the status part
        return jsonify({service: status})

    # Otherwise return statuses for all services
    statuses = {}
    for s, s_type in SERVICES.items():
        status = run_systemctl_command("is-active", s, s_type)[1]  # Get only the status part
        statuses[s] = status
    return jsonify(statuses)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
