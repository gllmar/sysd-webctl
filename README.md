# sysd-webctl



## Getting started

get script absolut path

### get dependencies? (y/n)
sudo apt install python3-gunicorn python3-flask gunicorn



### Create service (y/n)
cat /etc/systemd/system/sysd-webgui.service 


```
[Unit]
Description=sysd-webgui
After=network.target

[Service]
User=root
WorkingDirectory=/home/pi/src/sysd-webctl. # set this from the script path without setup.sh
ExecStart=/usr/bin/gunicorn -w 4 -b 0.0.0.0:8010 app:app
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
```


### Create NGINX route in nginx/conf.d  (y/n)

    # Create an NGINX configuration file for code forwarding
    nginx_config_file="/etc/nginx/conf.d/code_forwarding"
    echo "Creating NGINX configuration for code forwarding..."
    cat <<EOL | sudo tee $nginx_config_file

    location ~ ^/sysd-webgui(/.*)?$ {
        # Use return for a straightforward redirect
       return 302 http://$host:8010;
    }
    location ~ ^/sysd(/.*)?$ {
        # Use return for a straightforward redirect
       return 302 http://$host:8010;
    }
    location ~ ^/systemd(/.*)?$ {
        # Use return for a straightforward redirect
        return 302 http://$host:8010;
    }

EOL

    # Test NGINX configuration and reload if successful
    if sudo nginx -t; then
        echo "Reloading NGINX..."
        sudo systemctl reload nginx
    else
        echo "Failed to configure NGINX. Please check the configuration."
    fi



### setup complete